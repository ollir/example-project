const fetch = require('jest-fetch-mock');
const request = require('supertest');

jest.setMock('node-fetch', fetch);

beforeAll(() => {
  process.env.COLORS_BACKEND_ADDRESS = 'testing:1234';
})

test('GET /color calls backend and passes response through', async () => {
  const app = require('../../server/index'); 

  const expected = {'name': 'testing', 'hex': '#testing'};

  fetch.mockResponse(JSON.stringify(expected));

  response = await request(app)
    .get('/color/13')
    .expect(200)
    .expect('Content-Type', /json/);

  expect(fetch).toHaveBeenCalledTimes(1);
  expect(fetch).toHaveBeenCalledWith('http://testing:1234/color/13');

  expect(response.body).toEqual(expected);
});
